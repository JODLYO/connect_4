import numpy as np

def drawing_move(board):
    if np.sum(board == 0) == 0:
        return True
    else:
        return False
    
def winning_move(board, winner = 1):
	# Check horizontal locations for win
    for c in range(board.shape[1]-3):
        for r in range(board.shape[0]):
            if board[r][c] == winner and board[r][c+1] == winner and board[r][c+2] == winner and board[r][c+3] == winner:
                return True

	# Check vertical locations for win
    for c in range(board.shape[1]):
        for r in range(board.shape[0]-3):
            if board[r][c] == winner and board[r+1][c] == winner and board[r+2][c] == winner and board[r+3][c] == winner:
                return True

	# Check positively sloped diaganols
    for c in range(board.shape[1]-3):
        for r in range(board.shape[0]-3):
            if board[r][c] == winner and board[r+1][c+1] == winner and board[r+2][c+2] == winner and board[r+3][c+3] == winner:
                return True

	# Check negatively sloped diaganols
    for c in range(board.shape[1]-3):
        for r in range(3, board.shape[0]):
            if board[r][c] == winner and board[r-1][c+1] == winner and board[r-2][c+2] == winner and board[r-3][c+3] == winner:
                return True
    
def get_available_moves(state):
    available_moves = state[0] == 0
    available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
    return available_moves

def calculate_row(move, board):
    row_count = board.shape[0]
    for row in range(row_count): #selects correct row
        row = row_count - row - 1
        if board[row][move] == 0:
            return row
        
def is_game_over(board, winner, player):
    if drawing_move(board):
        action_value = 0.5
        return action_value
    elif winning_move(board, player):
        action_value = winner
        return action_value
    
def flip_ones_and_twos(matrix):
    matrix *= -1
    matrix += 3
    matrix = np.where(matrix == 3, 0, matrix)
    return matrix

def my_agent(state, static_agent):
    state_temp = state.copy()
    state_temp = flip_ones_and_twos(state_temp)
    action, _, _ = static_agent.act_negamax_3(state_temp)
    return action

def is_game_terminal(board):
    terminal = False
    if winning_move(board, 1):
        terminal = True
    elif is_game_over(board, True, 2):
        terminal = True
    elif drawing_move(board):
        terminal = True
    return terminal

def minimax(board, depth_temp, depth, maximising_player):
    dict = {}
    available_moves = get_available_moves(board)
    if depth_temp == 0 or is_game_terminal(board):
        #print(board)
        if winning_move(board, 1):
            return None, 1000000, None
        elif winning_move(board, 2):
            return None, -1000000, None
        elif drawing_move(board):
            return None, 0, None
        else:
            return None, 0.51, None
    if maximising_player:
        chosen_move = np.random.choice(available_moves)
        chosen_value = -np.inf
        for move in available_moves:
            row = calculate_row(move, board)
            board_next_move = board.copy()
            board_next_move[row][move] = 1
            value = minimax(board_next_move, depth_temp - 1, depth, False)[1]
            if depth_temp == depth:
                dict[move] = value
            if value > chosen_value:
                chosen_value = value
                chosen_move = move
        return chosen_move, chosen_value, dict
    else:
        chosen_move = np.random.choice(available_moves)
        chosen_value = np.inf
        for move in available_moves:
            row = calculate_row(move, board)
            board_next_move = board.copy()
            board_next_move[row][move] = 2
            value = minimax(board_next_move, depth_temp - 1, depth, True)[1]
            if value < chosen_value:
                chosen_value = value
                chosen_move = move
        return chosen_move, chosen_value, dict

def choose_key_from_values(q_max_val_dict, n):
    for k, v in q_max_val_dict.items():
        q_max_val_dict[k] = v ** n 
    action = np.random.choice(list(q_max_val_dict.keys()),
                p = np.array(list(q_max_val_dict.values())) / sum(q_max_val_dict.values()))
    return action

def choose_max_value(q_max_val_dict):
    return np.argmax(q_max_val_dict)


def agent_fin_play(state, agent):
    depth = 5
    action, action_dict = agent.get_action_using_minimax_q_learning(np.squeeze(state),
                                                                    depth, depth,
                                                                    True)
    return action, action_dict


