import numpy as np
#!pip install pygame
import pygame
import sys

BLUE = (0,0,200)
RED = (200, 0, 0)
BLACK = (0, 0, 0)
SQUARESIZE = 100
RADIUS = int(SQUARESIZE/2 - 5)
YELLOW = (200, 200, 0)

ROW_COUNT = 6
COLUMN_COUNT = 7

def create_board():
    board = np.zeros((ROW_COUNT, COLUMN_COUNT))
    return board

def drop_peice(board, row, selection, piece):
    board[row][selection] = piece
    return None


def valid_input(board, selection):
    return board[0][selection] == 0

def get_next_open_row(board, selection):
    for row in range(ROW_COUNT):
        row = ROW_COUNT - row - 1
        if board[row][selection] == 0:
            return row
        
def winning_move(board, piece):
	# Check horizontal locations for win
	for c in range(COLUMN_COUNT-3):
		for r in range(ROW_COUNT):
			if board[r][c] == piece and board[r][c+1] == piece and board[r][c+2] == piece and board[r][c+3] == piece:
				return True

	# Check vertical locations for win
	for c in range(COLUMN_COUNT):
		for r in range(ROW_COUNT-3):
			if board[r][c] == piece and board[r+1][c] == piece and board[r+2][c] == piece and board[r+3][c] == piece:
				return True

	# Check positively sloped diaganols
	for c in range(COLUMN_COUNT-3):
		for r in range(ROW_COUNT-3):
			if board[r][c] == piece and board[r+1][c+1] == piece and board[r+2][c+2] == piece and board[r+3][c+3] == piece:
				return True

	# Check negatively sloped diaganols
	for c in range(COLUMN_COUNT-3):
		for r in range(3, ROW_COUNT):
			if board[r][c] == piece and board[r-1][c+1] == piece and board[r-2][c+2] == piece and board[r-3][c+3] == piece:
				return True
            
def draw_board(board):
    for c in range(COLUMN_COUNT):
        for r in range(ROW_COUNT):
            pygame.draw.rect(screen, BLUE, (c*SQUARESIZE, r*SQUARESIZE + SQUARESIZE, SQUARESIZE, SQUARESIZE))
            pygame.draw.circle(screen, BLACK, (int(c*SQUARESIZE + SQUARESIZE/2), int(r*SQUARESIZE + SQUARESIZE*3/2)), RADIUS)
#%%
pygame.init()

width = COLUMN_COUNT * SQUARESIZE
height = (ROW_COUNT + 1) * SQUARESIZE
size = (width, height)

screen = pygame.display.set_mode(size)

board = create_board()
draw_board(board)
pygame.display.update()

done = False
turn = 0
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.MOUSEMOTION:
            pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))
            posx = event.pos[0]
            if turn == 0:
                pygame.draw.circle(screen, RED, (posx, int(SQUARESIZE/2)), RADIUS)
            else: 
                pygame.draw.circle(screen, YELLOW, (posx, int(SQUARESIZE/2)), RADIUS)
            pygame.display.update()
        if event.type == pygame.MOUSEBUTTONDOWN:
            # print(event.pos)
            #player 1 input
            if turn == 0:
                posx = event.pos[0]
                player_peice = 1
                #selection = int(input("Player 1 your turn (0-6)"))
                selection = int(posx/SQUARESIZE)
                if valid_input(board, selection):
                    row = get_next_open_row(board, selection)
                    pygame.draw.circle(screen, RED, (int(selection*SQUARESIZE + SQUARESIZE/2), int(row*SQUARESIZE + SQUARESIZE*3/2)), RADIUS)
                    pygame.display.update()
                    drop_peice(board, row, selection, player_peice)
                    if winning_move(board, player_peice):
                        print('player 2 loses')
                        done = True
            else:
                selection = int(posx/SQUARESIZE)
                player_peice = 2
                selection = int(posx/SQUARESIZE)
                if valid_input(board, selection):
                    row = get_next_open_row(board, selection)
                    pygame.draw.circle(screen, YELLOW, (int(selection*SQUARESIZE + SQUARESIZE/2), int(row*SQUARESIZE + SQUARESIZE*3/2)), RADIUS)
                    pygame.display.update()
                    drop_peice(board, row, selection, player_peice)
                    if winning_move(board, player_peice):
                        print('player 1 loses')
                        done = True
            turn += 1
            turn = turn % 2
            print(board)
                
                
#%%
            
class connect_4_env:
    def __init__(self, ROW_COUNT, COLUMN_COUNT, SQUARESIZE):
        self.row_count = ROW_COUNT
        self.column_count = COLUMN_COUNT
        self.square_size = SQUARESIZE
        self.board = self.create_board()
        self.width = COLUMN_COUNT * SQUARESIZE
        self.height = (ROW_COUNT + 1) * SQUARESIZE
        self.size = (self.width, self.height)
        self.blue = (0, 0, 200)
        self.black = (0, 0, 0)
        self.radius = int(SQUARESIZE/2 - 5)
        self.done = False
        self.red = (200, 0, 0)
        self.yellow = (200, 200, 0)

        
    def create_board(self):
        board = np.zeros((self.row_count, self.column_count))
        return board
                
    def draw_board(self, screen):
        for c in range(self.column_count):
            for r in range(self.row_count):
                pygame.draw.rect(screen, self.blue, (c*self.square_size, r*self.square_size + self.square_size, self.square_size, self.square_size))
                pygame.draw.circle(screen, self.black, (int(c*self.square_size + self.square_size/2), int(r*self.square_size + self.square_size*3/2)), self.radius)
                
    def get_next_open_row(self, board, selection):
        for row in range(ROW_COUNT):
            row = ROW_COUNT - row - 1
            if board[row][selection] == 0:
                return row
                
    def human_player(self, event, player_colour):
        if event.type == pygame.MOUSEMOTION:
            pygame.draw.rect(screen, self.black, (0,0, self.width, self.square_size))
            posx = event.pos[0]
            pygame.draw.circle(screen, player_colour, (posx, int(self.square_size/2)), self.radius)
            pygame.display.update()
        if event.type == pygame.MOUSEBUTTONDOWN:
            # print(event.pos)
            #player 1 input
            posx = event.pos[0]
            player_peice = 1
            #selection = int(input("Player 1 your turn (0-6)"))
            selection = int(posx/self.square_size)
            if valid_input(self.board, selection):
                row = get_next_open_row(self.board, selection)
                pygame.draw.circle(screen, self.red, (int(selection*self.square_size + self.square_size/2), int(row*self.square_size + self.square_size*3/2)), self.radius)
                pygame.display.update()
                drop_peice(self.board, row, selection, player_peice)
                if winning_move(self.board, player_peice):
                    print('winrar')
                    self.done = True
                    
    def play_game(self, player_1, player_2):
        screen = pygame.display.set_mode(self.size)
        draw_board(self, screen)
        turn = 0
        while not self.done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.MOUSEMOTION:
                    pygame.draw.rect(screen, BLACK, (0,0, width, SQUARESIZE))
                    posx = event.pos[0]
                    if turn == 0:
                        pygame.draw.circle(screen, RED, (posx, int(SQUARESIZE/2)), RADIUS)
                    else: 
                        pygame.draw.circle(screen, YELLOW, (posx, int(SQUARESIZE/2)), RADIUS)
                    pygame.display.update()
                if turn == 0:
                    if player_1 == 'human':
                        self.human_player(event, self.blue)
                if turn == 1:
                    if player_2 == 'human':
                        self.human_player(event, self.yellow)
        
        
env = connect_4_env(6, 7, 100)

env.play_game('human', 'human')
            
