import pandas as pd
import numpy as np
import gym
import tensorflow as tf
import matplotlib.pyplot as plt
from tqdm import tqdm
from kaggle_environments import evaluate, make, utils
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import Reshape
import matplotlib.pyplot as plt
from keras.optimizers import Adam
from scipy.special import softmax
from keras import layers, models, losses
import useful_functions_c4

def get_available_moves(state):
    available_moves = state[0] == 0
    available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
    return available_moves

def flip_ones_and_twos(matrix):
    matrix *= -1
    matrix += 3
    matrix = np.where(matrix == 3, 0, matrix)
    return matrix

def drawing_move(board):
    if np.sum(board == 0) == 0:
        return True
    else:
        return False
    
def calculate_row(move, board):
    row_count = board.shape[0]
    for row in range(row_count): #selects correct row
        row = row_count - row - 1
        if board[row][move] == 0:
            return row
        
def is_game_over(board, win_value, player = 1):
    if drawing_move(board):
        action_value = 0.5
        return action_value
    elif winning_move(board, player):
        action_value = win_value
        return action_value
    
def winning_move(board, winner = 1):
	# Check horizontal locations for win
    for c in range(board.shape[1]-3):
        for r in range(board.shape[0]):
            if board[r][c] == winner and board[r][c+1] == winner and board[r][c+2] == winner and board[r][c+3] == winner:
                return True

	# Check vertical locations for win
    for c in range(board.shape[1]):
        for r in range(board.shape[0]-3):
            if board[r][c] == winner and board[r+1][c] == winner and board[r+2][c] == winner and board[r+3][c] == winner:
                return True

	# Check positively sloped diaganols
    for c in range(board.shape[1]-3):
        for r in range(board.shape[0]-3):
            if board[r][c] == winner and board[r+1][c+1] == winner and board[r+2][c+2] == winner and board[r+3][c+3] == winner:
                return True

	# Check negatively sloped diaganols
    for c in range(board.shape[1]-3):
        for r in range(3, board.shape[0]):
            if board[r][c] == winner and board[r-1][c+1] == winner and board[r-2][c+2] == winner and board[r-3][c+3] == winner:
                return True

class ReplayBuffer:
    def __init__(self, size, minibatch_size):
        """
        Args:
            size (integer): The size of the replay buffer.              
            minibatch_size (integer): The sample size.
        """
        self.buffer = []
        self.minibatch_size = minibatch_size
        self.max_size = size

    def append(self, state, action, reward, next_state, terminal):
        """
        Args:
            state (Numpy array): The state.              
            action (integer): The action.
            reward (float): The reward.
            terminal (integer): 1 if the next state is a terminal state and 0 otherwise.
            next_state (Numpy array): The next state.           
        """
        if len(self.buffer) == self.max_size:
            del self.buffer[0]
        self.buffer.append([state, action, reward, next_state, terminal])

    def sample(self):
        """
        Returns:
            A list of transition tuples including state, action, reward, terinal, and next_state
        """
        idxs = np.random.choice(np.arange(len(self.buffer)), size=self.minibatch_size)
        return [self.buffer[idx] for idx in idxs]

    def size(self): #try changing to __len__
        return len(self.buffer)
    
class Agent:
    def __init__(self, state_size, action_size):
        #self.state_size = env.columns * env.rows
        #self.action_size = env.columns
        self.state_size = state_size
        self.action_size = action_size
        self.experience_replay = ReplayBuffer(size = 10000, minibatch_size = 32)
        self.gamma = 1
        self.q_network = self.create_nn()
        self.target_network = self.create_nn()
        self.target_network.set_weights(self.q_network.get_weights())
        self.epsilon = 0.1
        self.player_1 = 1
        self.player_2 = 2
    
    def create_nn(self):
        model = models.Sequential()
        model.add(layers.Conv2D(8, (4, 4), activation='relu', input_shape=(6, 7, 1), padding = 'same'))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(16, (4, 4), activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Conv2D(32, (3, 3), activation='relu'))
        model.add(layers.BatchNormalization())
        model.add(layers.Flatten())
        model.add(layers.Dense(7, activation='sigmoid'))
        model.compile(loss=losses.BinaryCrossentropy(), optimizer = Adam())        
        model.build((None, self.state_size))
        return model
    
    def load_nn(self, path):
        self.q_network.load_weights(path)

    def act_epsilon_greedy(self, state):
        if np.random.rand() <= self.epsilon:
            return np.random.choice([c for c in range(self.action_size) if state[0][c] == 0])
        else:
            q_values = self.q_network.predict(state)
            for i in range(self.action_size):
                if state[0][i] != 0:
                    q_values[0][i] = -1e7
            return np.argmax(q_values[0])
        
    def act(self, state):
        #state = state.reshape(state.shape + (1,))
        q_values = self.q_network.predict([[state]])
        for i in range(self.action_size):
            if state[0][i] != 0:
                q_values[0][i] = -1e7
        probabilities = softmax(q_values)[0]
        action = np.random.choice(range(self.action_size), 1, p=probabilities)[0]
        return action
    
    def act_negamax_2(self, state, row_count = 6):
        available_moves = state[0] == 0
        available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
        board = np.squeeze(state)
        q_max_val_dict = {}
        for move in available_moves:
            board_temp = board.copy()
            for row in range(row_count): #selects correct row
                row = row_count - row - 1
                if board[row][move] == 0:
                    break
            board_temp[row][move] = self.player_1
            if drawing_move(board_temp):
                action_value = 0.5
                return move, action_value
            elif winning_move(board_temp):
                action_value = 1
                return move, action_value
            board_temp = flip_ones_and_twos(board_temp)
            state_temp = board_temp.reshape(board_temp.shape + (1,))
            q_max = np.amax(1 - self.q_network.predict([[state_temp]]))
            q_max_val_dict[str(move)] = q_max
        for key in q_max_val_dict:
            q_max_val_dict[key] = 1 - q_max_val_dict[key]
        action = max(q_max_val_dict, key = q_max_val_dict.get)
        action_value = q_max_val_dict[action]
        return action, action_value
    
    def act_negamax_3(self, state):
        available_moves = get_available_moves(state)
        board_temp = np.squeeze(state)
        board = board_temp.copy()
        q_max_val_dict = {}
        for move in available_moves:
            board_temp = board.copy()
            row = calculate_row(move, board_temp)
            board_temp[row][move] = 1
            action_value = is_game_over(board_temp, 1)#!TODO check game_over function
            if action_value:
                return move, action_value, q_max_val_dict
            available_moves = get_available_moves(board_temp)
            ply_2_eval = {}
            for move_ply_2 in available_moves:
                board_temp_ply_2 = board_temp.copy()
                row_ply_2 = calculate_row(move_ply_2, board_temp_ply_2)
                board_temp_ply_2[row_ply_2][move_ply_2] = 2
                action_value = is_game_over(board_temp_ply_2, 0, 2)
                if action_value is not None:
                    ply_2_eval[move_ply_2] = action_value
                    continue
                state_temp_ply_2 = board_temp_ply_2.reshape(board_temp_ply_2.shape + (1,))
                q_max = np.amax(self.q_network.predict([[state_temp_ply_2]]))
                ply_2_eval[move_ply_2] = q_max
            move_ply_2 = min(ply_2_eval, key = ply_2_eval.get)
            q_move_ply_2 = ply_2_eval[move_ply_2]
            q_max_val_dict[move] = q_move_ply_2
        action = max(q_max_val_dict, key = q_max_val_dict.get)
        action_value = q_max_val_dict[action]
        return action, action_value, q_max_val_dict
    
    def minimax(self, board, depth_temp, depth, maximising_player):
        dict = {}
        available_moves = get_available_moves(board)
        if depth_temp == 0 or useful_functions_c4.is_game_terminal(board):
            #print(board)
            if winning_move(board, 1):
                return None, 1000000, None
            elif winning_move(board, 2):
                return None, -1000000, None
            elif drawing_move(board):
                return None, 0, None
            else:
                return None, 0.51, None
        if maximising_player:
            chosen_move = np.random.choice(available_moves)
            chosen_value = -np.inf
            for move in available_moves:
                row = calculate_row(move, board)
                board_next_move = board.copy()
                board_next_move[row][move] = 1
                value = self.minimax(board_next_move, depth_temp - 1, depth, False)[1]
                if depth_temp == depth:
                    dict[move] = value
                if value > chosen_value:
                    chosen_value = value
                    chosen_move = move
            return chosen_move, chosen_value, dict
        else:
            chosen_move = np.random.choice(available_moves)
            chosen_value = np.inf
            for move in available_moves:
                row = calculate_row(move, board)
                board_next_move = board.copy()
                board_next_move[row][move] = 2
                value = self.minimax(board_next_move, depth_temp - 1, depth, True)[1]
                if value < chosen_value:
                    chosen_value = value
                    chosen_move = move
            return chosen_move, chosen_value, dict
        
    def get_action_using_minimax_q_learning(self, board, depth_temp, depth, maximising_player):
        chosen_move, chosen_value, dict = self.minimax(board, depth_temp, depth, maximising_player)
        minimax_action_values = pd.DataFrame(dict.values(), index = dict.keys())
        if (minimax_action_values == 0.51).sum()[0] == len(minimax_action_values):
            action, action_value, q_max_val_dict = self.act_negamax_3(board)
        else:
            action = max(dict, key = dict.get)
            q_max_val_dict = None
        return action, q_max_val_dict
    
    def retrain(self, batch_size): #TODO! doesn't need batch_size
        minibatch = self.experience_replay.sample()
        
        for state, action, reward, next_state, terminated in minibatch:
            
            target = self.q_network.predict([[state]])
            
            if terminated:
                target[0][action] = reward
            else:
                t = self.target_network.predict([[next_state]])
                target[0][action] = reward + self.gamma * np.amax(t)
            
            self.q_network.fit([[state]], target, epochs=1, verbose=0)
            
    def retrain_negamax(self, batch_size):
        minibatch = self.experience_replay.sample()
        
        for state, action, reward, next_state, terminated in minibatch:
            
            target = self.q_network.predict([[state]])
            
            if terminated:
                target[0][action] = reward
                print('reward is {}'.format(reward))
            else:
                try:
                    #_, td_err, _ = self.act_negamax_3(next_state)
                    sarsa = np.mean(self.q_network.predict([[next_state]])[0])
                    print(sarsa)
                    target[0][action] = reward + self.gamma * sarsa
                except ValueError:
                    print(np.squeeze(state))
                    print(np.squeeze(next_state))
                    print(terminated)
                    print(drawing_move(np.squeeze(state)))
                    print(reward)
                    raise ValueError('board ran out of space in while training')
                
            
            self.q_network.fit([[state]], target, epochs=1, verbose=0)
            
            
            
            