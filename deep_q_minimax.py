from pathlib import Path
import os
import numpy as np

from tensorflow.keras import layers, models


connect_4_path = Path('/Users/joe.odonnell/Desktop/connect4/connect_4')
os.chdir(connect_4_path)

from env_class import connect_4_env


def my_random_agent(observation, configuration):
    available_moves = observation.state[0] == 0
    available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
    return np.random.choice(available_moves)

def my_connect_4_convnet():
    model = models.Sequential()
    model.add(layers.Conv2D(8, (4, 4), activation='relu', input_shape=(6, 7, 1), padding = 'same'))
    model.add(layers.Conv2D(16, (4, 4), activation='relu'))
    model.add(layers.Conv2D(32, (3, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(7, activation='sigmoid'))
    return model


def obs_to_network_input(observation, player):
    board = observation.state
    if player == 2: #switch ones and twos around
        board *= -1
        board += 3
        board = np.where(board == 3, 0, board)
    return board

board = np.zeros((6, 7))
board[5, 3] = 1
board[5, 4] = 2

def one_step_minimax_q_targets(next_q_vals, experience):
  next_q_minimax_star = (1-next_q_vals.max(1)).tolist() # Negamax - 2 player 0-sum
  terminal_rewards = [e.episode_reward for e in experience]
  last_episode_actions = [e.last_episode_action for e in experience]
  target_qs = np.array([t if l else n for(t, l, n) in zip(
    terminal_rewards, last_episode_actions, next_q_minimax_star)])
  return target_qs

model.predict(np.zeros((10, 6, 7, 1)))

def minimax_q_learning(model, experience, nan_coding_value):
  # Evaluate the Q-values of the current and next state for all observations
  num_actions = experience[0].current_network_input.shape[1]
  num_steps = len(experience)
  current_states = np.stack([e.current_network_input for e in experience])
  next_states = np.stack([e.next_network_input for e in experience])
  current_q_vals = model.predict(current_states)
  next_q_vals = model.predict(next_states)
    
  # Filter out next Q-values where the next action is not valid. Filtered out
  # since every target computation performs a max operation.
  # This was an unintended bug in the original version of the notebook!
  #available_moves = next_states[0] == 0 !TODO make this
  #next_q_vals[next_states[:, 0, :, 0] == 0] = -1

  # Compute the target Q-values
  target_qs = one_step_minimax_q_targets(next_q_vals, experience)

  
  # Don't learn about non acted Q-values
  all_target_qs = nan_coding_value*np.ones([num_steps, num_actions])
    
  # Set the targets for the actions that were selected
  actions = [e.action for e in experience]
  all_target_qs[np.arange(num_steps), actions] = target_qs
    
  return current_states, all_target_qs