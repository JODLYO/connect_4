#!pip install 'kaggle-environments>=0.1.6' > /dev/null 2>&1
#!pip install gym
import pandas as pd
import numpy as np
import gym
import tensorflow as tf
import matplotlib.pyplot as plt
from tqdm import tqdm
from kaggle_environments import evaluate, make, utils
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import Reshape
import matplotlib.pyplot as plt
from keras.optimizers import Adam
from scipy.special import softmax



class ReplayBuffer:
    def __init__(self, size, minibatch_size):
        """
        Args:
            size (integer): The size of the replay buffer.              
            minibatch_size (integer): The sample size.
        """
        self.buffer = []
        self.minibatch_size = minibatch_size
        self.max_size = size

    def append(self, state, action, reward, terminal, next_state):
        """
        Args:
            state (Numpy array): The state.              
            action (integer): The action.
            reward (float): The reward.
            terminal (integer): 1 if the next state is a terminal state and 0 otherwise.
            next_state (Numpy array): The next state.           
        """
        if len(self.buffer) == self.max_size:
            del self.buffer[0]
        self.buffer.append([state, action, reward, next_state, terminal])

    def sample(self):
        """
        Returns:
            A list of transition tuples including state, action, reward, terinal, and next_state
        """
        idxs = np.random.choice(np.arange(len(self.buffer)), size=self.minibatch_size)
        return [self.buffer[idx] for idx in idxs]

    def size(self): #try changing to __len__
        return len(self.buffer)
    
class Agent:
    def __init__(self, state_size, action_size):
        #self.state_size = env.columns * env.rows
        #self.action_size = env.columns
        self.state_size = state_size
        self.action_size = action_size
        self.experience_replay = ReplayBuffer(size = max_experiences, minibatch_size = batch_size)
        self.gamma = 0.99
        self.q_network = self.create_nn()
        self.target_network = self.create_nn()
        self.target_network.set_weights(self.q_network.get_weights())
        self.epsilon = 0.1
    
    def create_nn(self):
        model = models.Sequential()
        model.add(layers.Conv2D(8, (4, 4), activation='relu', input_shape=(6, 7, 1), padding = 'same'))
        model.add(layers.Conv2D(16, (4, 4), activation='relu'))
        model.add(layers.Conv2D(32, (3, 3), activation='relu'))
        model.add(layers.Flatten())
        model.add(layers.Dense(7, activation='sigmoid'))
        model.compile(loss='mse', optimizer = Adam())        
        model.build((None, self.state_size))
        return model
    
    def load_nn(self, path):
        self.q_network.load_weights(path)

    def act_epsilon_greedy(self, state):
        if np.random.rand() <= self.epsilon:
            return np.random.choice([c for c in range(self.action_size) if state[0][c] == 0])
        else:
            q_values = self.q_network.predict(state)
            for i in range(self.action_size):
                if state[0][i] != 0:
                    q_values[0][i] = -1e7
            return np.argmax(q_values[0])
    def act(self, state):
        q_values =  self.q_network.predict(state)
        # for i in range(self.action_size):
        #     if state[0][i] != 0:
        #         q_values[0][i] = -1e7
        probabilities = softmax(q_values)[0]
        action = np.random.choice(range(self.action_size), 1, p=probabilities)[0]
        return action
    
    def retrain(self, batch_size): #TODO! doesn't need batch_size
        minibatch = self.experience_replay.sample()
        
        for state, action, reward, next_state, terminated in minibatch:
            
            target = self.q_network.predict([[state]])
            
            if terminated:
                target[0][action] = reward
            else:
                t = self.target_network.predict([[next_state]])
                target[0][action] = reward + self.gamma * np.amax(t)
            
            self.q_network.fit([[state]], target, epochs=1, verbose=0)
    
class ConnectX(gym.Env):
    def __init__(self, switch_prob=0):
        self.env = make('connectx', debug=True)
        self.pair = [None, 'random']
        self.trainer = self.env.train(self.pair)
        self.switch_prob = switch_prob
        self.columns = self.env.configuration.columns
        self.rows = self.env.configuration.rows

        # Define required gym fields (examples):
        config = self.env.configuration
        self.action_space = gym.spaces.Discrete(config.columns)
        self.observation_space = gym.spaces.Discrete(config.columns * config.rows)

    def switch_trainer(self):
        self.pair = self.pair[::-1]
        self.trainer = self.env.train(self.pair)

    def step(self, action):
        return self.trainer.step(action)
    
    def reset(self):
        if np.random.random() < self.switch_prob:
            self.switch_trainer()
        return self.trainer.reset()
    
    def render(self, **kwargs):
        return self.env.render(**kwargs)
    
    def run(self, agent1, agent2):
        return self.env.run([agent1, agent2])

#%%
max_experiences = 10000
batch_size = 32
no_episodes = 10000

env = ConnectX()
agent_test = Agent(env)
agent_test.load_nn('/Users/joe.odonnell/Desktop/connect4/connect_4/wieghts.h5')

#%%
all_avg_rewards = np.empty(no_episodes)
for e in tqdm(range(no_episodes)):
    agent_test.target_network.set_weights(agent_test.q_network.get_weights())
    observations = env.reset()
    done = False
    
    while not done:
        state = np.array([observations['board']])
        action = int(agent_test.act(state))
        prev_state = state
        observations, reward, done, _ = env.step(action)
        state = np.array([observations['board']])
        if done:
            all_avg_rewards[e] = reward

        agent_test.experience_replay.append(prev_state, action, reward, done, state)
        
        if len(agent_test.experience_replay.buffer) >= batch_size:
            agent_test.retrain(batch_size)

#%%
plt.plot(pd.Series(all_avg_rewards).cumsum())
agent_test.q_network.save_weights('/Users/joe.odonnell/Desktop/connect4/connect_4/wieghts.h5')

plt.plot(pd.DataFrame(all_avg_rewards))


model = Sequential()
model.add(Dense(42, activation = 'relu'))
model.add(Dense(50, activation='relu'))
model.add(Dense(50, activation='relu'))
model.add(Dense(7, activation='linear'))

model.compile(loss='mse', optimizer = Adam())
model.build((None, 42))
model.summary()

model.load_weights('/Users/joe.odonnell/Desktop/connect4/connect_4/wieghts.h5')


q_network = agent_test.q_network
q_network = model

def myagent(observation, configuration):
    state = np.array([observations['board']])
    q_values = q_network.predict(state)
    for i in range(configuration.columns):
        if state[0][i] != 0:
            q_values[0][i] = -1e7
    return np.argmax(q_values[0])
    

from operator import itemgetter

def grade_agent(game_name, agent, opponent, episodes):
    as_p1 = evaluate(game_name, [agent, opponent], num_episodes=episodes)
    as_p1_reward = sum(map(itemgetter(0), as_p1))
    as_p1_total = sum(map(itemgetter(1), as_p1)) + as_p1_reward
    
    as_p2 = evaluate(game_name, [opponent, agent], num_episodes=episodes)
    as_p2_reward = sum(map(itemgetter(1), as_p2))
    as_p2_total = sum(map(itemgetter(0), as_p2)) + as_p2_reward
    
    return 100 * (as_p1_reward + as_p2_reward) / (as_p1_total + as_p2_total)

evaluate("connectx", [myagent, "random"], num_episodes = 1)
env.run(myagent, "random")
env.render()

if -1:
    print(1)

reward = 0
terminated = False

action = agent_test.act(state)

observations, reward, done, _ = env.train([None, 'random']).step(action)


env = make('connectx', debug = True)
env.state[0].observation.board
env.state[0].observation.mark

state[0][41]

env.configuration
env.observation_space.n
config = env.configuration
gym.spaces.Discrete(config.columns * config.rows)


replay_buffer = ReplayBuffer(100, 10)

model = Sequential()

model.add(Dense(units=64, activation='relu', input_dim=100))
model.add(Dense(units=10, activation='softmax'))


softmax([1,2,3])


import keras
keras.__version__


#%% lunar lander
!pip install Box2D
!pip install gym
env = gym.make('CartPole-v1')

total = 0
max_experiences = 500
batch_size = 32
no_episodes = 1000

my_agent = Agent(env.observation_space.shape[0], env.action_space.n)


all_avg_rewards = []
for e in tqdm(range(no_episodes)):
    my_agent.target_network.set_weights(my_agent.q_network.get_weights())
    observations = env.reset()
    done = False
    all_avg_rewards.append(total)
    while not done:
        state = np.array(observations)
        action = int(my_agent.act([[state]]))
        prev_state = state
        observations, reward, done, _ = env.step(action)
        total += reward
        state = np.array(observations)
        # if done:
        #     all_avg_rewards[e] = reward

        my_agent.experience_replay.append(prev_state, action, reward, done, state)
        
        if len(my_agent.experience_replay.buffer) >= batch_size:
            my_agent.retrain(batch_size)


env.render()

plt.plot(all_avg_rewards)

plt.plot(pd.Series([all_avg_rewards[i] - all_avg_rewards[i-1] for i in range(len(all_avg_rewards)) if i !=0]))
