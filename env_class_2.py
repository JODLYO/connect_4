import numpy as np
#!pip install pygame
import pygame
import sys
import time

class connect_4_env:
    def __init__(self, ROW_COUNT, COLUMN_COUNT, SQUARESIZE):
        self.row_count = ROW_COUNT
        self.column_count = COLUMN_COUNT
        self.square_size = SQUARESIZE
        self.board = self.create_board()
        self.width = COLUMN_COUNT * SQUARESIZE
        self.height = (ROW_COUNT + 1) * SQUARESIZE
        self.size = (self.width, self.height)
        self.blue = (0, 0, 200)
        self.black = (0, 0, 0)
        self.radius = int(SQUARESIZE/2 - 5)
        self.done = False
        self.player_1_colour = (200, 0, 0) #red
        self.player_2_colour = (200, 200, 0) #yellow
        self.player = 1
        self.current_colour = self.player_1_colour

        
    def create_board(self):
        board = np.zeros((self.row_count, self.column_count))
        return board
    
    def valid_input(self, selection):
        return self.board[0][selection] == 0
    
    def drop_peice(self, selection):
        row = self.get_next_open_row(selection)
        pygame.draw.circle(self.screen, self.current_colour, (int(selection*self.square_size + self.square_size/2), int(row*self.square_size + self.square_size*3/2)), self.radius)
        self.board[row][selection] = self.player
        return None
                
    def draw_board(self):
        for c in range(self.column_count):
            for r in range(self.row_count):
                pygame.draw.rect(self.screen, self.blue, (c*self.square_size, r*self.square_size + self.square_size, self.square_size, self.square_size))
                pygame.draw.circle(self.screen, self.black, (int(c*self.square_size + self.square_size/2), int(r*self.square_size + self.square_size*3/2)), self.radius)
                
    def winning_move(self):
    	# Check horizontal locations for win
    	for c in range(self.column_count-3):
    		for r in range(self.row_count):
    			if self.board[r][c] == self.player and self.board[r][c+1] == self.player and self.board[r][c+2] == self.player and self.board[r][c+3] == self.player:
    				return True
    
    	# Check vertical locations for win
    	for c in range(self.column_count):
    		for r in range(self.row_count-3):
    			if self.board[r][c] == self.player and self.board[r+1][c] == self.player and self.board[r+2][c] == self.player and self.board[r+3][c] == self.player:
    				return True
    
    	# Check positively sloped diaganols
    	for c in range(self.column_count-3):
    		for r in range(self.row_count-3):
    			if self.board[r][c] == self.player and self.board[r+1][c+1] == self.player and self.board[r+2][c+2] == self.player and self.board[r+3][c+3] == self.player:
    				return True
    
    	# Check negatively sloped diaganols
    	for c in range(self.column_count-3):
    		for r in range(3, self.row_count):
    			if self.board[r][c] == self.player and self.board[r-1][c+1] == self.player and self.board[r-2][c+2] == self.player and self.board[r-3][c+3] == self.player:
    				return True
                
    def get_next_open_row(self, selection):
        for row in range(self.row_count):
            row = self.row_count - row - 1
            if self.board[row][selection] == 0:
                return row
    
    def move_position(self):
        pygame.draw.rect(self.screen, self.black, (0,0, self.width, self.square_size))
        pygame.draw.circle(self.screen, self.current_colour, (self.posx, int(self.square_size/2)), self.radius)
        pygame.display.update()
    
    def make_selection(self):
        selection = int(self.posx/self.square_size)
        return selection
    
    def update_piece_colour(self, self.posx):
        pygame.draw.circle(self.screen, self.current_colour, (self.posx, int(self.square_size/2)), self.radius)
        pygame.display.update()
    
    def change_player(self):
        self.player += 1
        self.player = self.player % 2
        if self.player == 0:
            self.player = 2
            self.current_colour = self.player_2_colour
        else:
            self.current_colour = self.player_1_colour
        print(self.board)
                
    def human_play(self, event):
        if event.type == pygame.MOUSEMOTION:
            self.posx = event.pos[0]
            self.move_position(self.posx)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.posx = event.pos[0]
            selection = self.make_selection()
            if self.valid_input(selection):
                self.drop_peice(selection)
                if self.winning_move():
                    print('winrar')
                    self.done = True
                self.change_player()
                self.update_piece_colour()

    def ai_play(self):
        available_moves = self.board[0] == 0
        available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
        selection = np.random.choice(available_moves)
        
        self.drop_peice(selection)
        if self.winning_move():
            print('winrar')
            self.done = True
        self.change_player()
                    
    def play_game(self, player_1, player_2):
        self.screen = pygame.display.set_mode(self.size)
        self.draw_board()
        while not self.done:
            for event in pygame.event.get():
                print('event is {} .get is {}'.format(event,  pygame.event.get()))
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.MOUSEMOTION:
                    self.posx = event.pos[0]
                    self.move_position(self.posx)
                if self.player == 1:
                    if player_1 == 'human':
                        self.human_play(event)
                    elif player_1 == 'ai':
                        self.ai_play()
                elif self.player == 2:
                    if player_2 == 'human':
                        self.human_play(event)
                    elif player_2 == 'ai':
                        self.ai_play()

        
        
env = connect_4_env(6, 7, 100)

env.play_game('ai', 'ai')
            