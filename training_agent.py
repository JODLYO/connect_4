import os
from env_class import connect_4_env
from agent_class import Agent, flip_ones_and_twos, drawing_move, is_game_over
import numpy as np
from tqdm import tqdm
from scipy.special import softmax
import pandas as pd
import useful_functions_c4


def drawing_move(board):
    if np.sum(board == 0) == 0:
        return True
    else:
        return False
    
def winning_move(board):
	# Check horizontal locations for win
	for c in range(board.shape[1]-3):
		for r in range(board.shape[0]):
			if board[r][c] == 1 and board[r][c+1] == 1 and board[r][c+2] == 1 and board[r][c+3] == 1:
				return True

	# Check vertical locations for win
	for c in range(board.shape[1]):
		for r in range(board.shape[0]-3):
			if board[r][c] == 1 and board[r+1][c] == 1 and board[r+2][c] == 1 and board[r+3][c] == 1:
				return True

	# Check positively sloped diaganols
	for c in range(board.shape[1]-3):
		for r in range(board.shape[0]-3):
			if board[r][c] == 1 and board[r+1][c+1] == 1 and board[r+2][c+2] == 1 and board[r+3][c+3] == 1:
				return True

	# Check negatively sloped diaganols
	for c in range(board.shape[1]-3):
		for r in range(3, board.shape[0]):
			if board[r][c] == 1 and board[r-1][c+1] == 1 and board[r-2][c+2] == 1 and board[r-3][c+3] == 1:
				return True
    
def get_available_moves(state):
    available_moves = state[0] == 0
    available_moves = [i for i in range(len(available_moves)) if available_moves[i]]
    return available_moves

def calculate_row(move, board):
    row_count = board.shape[0]
    for row in range(row_count): #selects correct row
        row = row_count - row - 1
        if board[row][move] == 0:
            return row
    
# def act_nagamax_3(state):
#     available_moves = get_available_moves(state)
#     board_temp = np.squeeze(state)
#     board = board_temp.copy()
#     board = flip_ones_and_twos(board) #delete after testing
#     q_max_val_dict = {}
#     for move in available_moves:
#         board_temp = board.copy()
#         row = calculate_row(move, board_temp)
#         board_temp[row][move] = 1
#         action_value = is_game_over(board_temp, 1)
#         if action_value:
#             return move, action_value
#         available_moves = get_available_moves(board_temp)
#         ply_2_eval = {}
#         for move_ply_2 in available_moves:
#             board_temp_ply_2 = board_temp.copy()
#             row_ply_2 = calculate_row(move_ply_2, board_temp_ply_2)
#             board_temp_ply_2[row_ply_2][move_ply_2] = 2
#             action_value = is_game_over(board_temp_ply_2, 0)
#             if action_value: #dont make return
#                 ply_2_eval[move_ply_2] = action_value
#                 continue
#             state_temp_ply_2 = board_temp_ply_2.reshape(board_temp_ply_2.shape + (1,))
#             q_max = np.amax(agent.q_network.predict([[state_temp_ply_2]]))
#             ply_2_eval[move_ply_2] = q_max
#         move_ply_2 = min(ply_2_eval, key = ply_2_eval.get)
#         q_move_ply_2 = ply_2_eval[move_ply_2]
#         q_max_val_dict[move] = q_move_ply_2
#     action = max(q_max_val_dict, key = q_max_val_dict.get)
#     action_value = q_max_val_dict[action]
#     return action, action_value, q_max_val_dict
    
def randomize_player_going_first(env, agent):
    if np.random.rand() < 0.5:
        env.change_player_be()
        action, _ = agent.get_action_using_minimax_q_learning(np.squeeze(state), depth, depth, maximising_player)
        env.drop_piece_be(action)
        env.change_player_be()
    return env

def select_agents_action(state, depth_minimax, move_randomness, agent):
    """
    Parameters
    ----------
    state : np array
        board state
    depth_minimax : int
        depth of minimax
    move_randomness : float
        lower values are more random

    Returns
    -------
    action : int

    """
    action, q_max_val_dict = agent.get_action_using_minimax_q_learning(np.squeeze(state),
                                                                       depth_minimax,
                                                                       depth_minimax,
                                                                       True)
    if q_max_val_dict is not None: #if not a forced move
        action = useful_functions_c4.choose_key_from_values(q_max_val_dict, move_randomness)
    return action

def select_agents_max_action_value(state, depth_minimax, agent):
    action, q_max_val_dict = agent.get_action_using_minimax_q_learning(np.squeeze(state),
                                                                       depth_minimax,
                                                                       depth_minimax,
                                                                       True)
    if q_max_val_dict is not None: #if not a forced move
        action = useful_functions_c4.choose_max_value(q_max_val_dict)
    return action
    

if __name__ == '__main__':
    depth = 5
    maximising_player = True
    max_experiences = 50000
    batch_size = 20
    no_episodes = 10000
    env = connect_4_env(6, 7, 100)
    agent = Agent(42, 7)
    agent.load_nn('conv_net_wieghts_try_2_552.h5')
    
    def my_agent(state, agent):
        state_temp = state.copy()
        state_temp = flip_ones_and_twos(state_temp)
        action, _ = agent.get_action_using_minimax_q_learning(np.squeeze(state), depth, depth, maximising_player)
        return action
    
    rewards = []
    for e in tqdm(range(no_episodes)):
        agent.target_network.set_weights(agent.q_network.get_weights()) #resets the target networks weights
        if e % 200 == 0: #save weights
            agent.q_network.save_weights('conv_net_wieghts_try_2_' + str(e) + '.h5')
        state, reward, done = env.reset()
        env = randomize_player_going_first(env, agent)
        while not done:
            action = select_agents_action(state, 5, 2, agent)
            prev_state = state.copy()
            state, reward, done = env.half_step(action)
            if not done:
                state = useful_functions_c4.flip_ones_and_twos(state.copy())
                action = select_agents_action(state, 5, 100, agent)#less random
                state, reward, done = env.half_step(action)
                state = useful_functions_c4.flip_ones_and_twos(state.copy())
                if reward == 1:
                    reward = 0
            if done:
                print(np.squeeze(state))
                print(reward)
                rewards.append(reward)
            agent.experience_replay.append(prev_state.copy(), action, reward, state.copy(), done)
            
            if len(agent.experience_replay.buffer) >= batch_size:
                agent.retrain_negamax(batch_size)
    agent.q_network.save_weights('conv_net_wieghts.h5')



# [[1. 0. 0. 0. 0. 0. 0.]
#  [2. 0. 0. 0. 0. 0. 1.]
#  [1. 2. 0. 0. 0. 2. 2.]
#  [1. 2. 1. 1. 0. 1. 2.]
#  [2. 1. 2. 2. 0. 1. 2.]
#  [2. 1. 1. 1. 2. 2. 1.]]

# [[1. 0. 0. 0. 0. 0. 0.]
#  [2. 1. 0. 0. 0. 0. 1.]
#  [1. 2. 0. 0. 0. 2. 2.]
#  [1. 2. 0. 1. 0. 1. 2.]
#  [2. 1. 2. 2. 0. 1. 2.]
#  [2. 1. 1. 1. 2. 2. 1.]]


